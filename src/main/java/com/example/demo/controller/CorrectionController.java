package com.example.demo.controller;

import com.example.demo.dto.CorrectionDto;
import com.example.demo.mapper.MapperCorrection;
import com.example.demo.model.Correction;
import com.example.demo.model.Member;
import com.example.demo.service.CorrectionService;
import com.example.demo.service.CorrectionServiceImpl;
import com.example.demo.service.MemberService;
import com.example.demo.service.MemberServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/corrections")
public class CorrectionController {
    @Autowired
    CorrectionService correctionService = new CorrectionServiceImpl();
    @Autowired
    MemberService memberService=new MemberServiceImpl();


    @PostMapping(value= "/addCorrection")
    public ResponseEntity<Correction> createCorrection(@RequestBody CorrectionDto correctionDto) {
        try {
            List<Member> members = this.memberService.findAllMember();
            Correction correction = new Correction();
            for (Member member : members) {
                if (member.getId().equals(correctionDto.getMemberId())) {
                    correction =MapperCorrection.correctionToCorrectionDto(correctionDto);
                    correction.setMembers(member);
                    break;
                }
            }
            return new ResponseEntity<>(this.correctionService.createCorrection(correction),HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping( value= "/listCorrection")
    public ResponseEntity<List<Correction>> findAllCorrection(){
        try {
            List<Correction> corrections = correctionService.findAllCorrection();
            return new ResponseEntity<>(corrections, HttpStatus.ACCEPTED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    @GetMapping( value="/listCorrection/{id}")
    public  ResponseEntity<Correction> findCorrectionById(@PathVariable("id") Integer id){
        try {
            Optional<Correction> correction=this.correctionService.findCorrectionById(id);
            if (correction.isPresent()){

                return new ResponseEntity(correction, HttpStatus.FOUND);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping(value = "/deleteCorrection/{id}")
    public ResponseEntity<Correction> deleteCorrectionById(@PathVariable Integer id){
        try {
            if (correctionService.findCorrectionById(id).isPresent()) {
                correctionService.deleteCorrectionById(id);
                return new ResponseEntity<>(HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping(value = "/updateCorrection/{id}")
    public ResponseEntity<Correction> updateCorrection(@RequestBody Correction correction, @PathVariable Integer id) {
        try {
            if(correctionService.findCorrectionById(id).isPresent()) {
                correctionService.findCorrectionById(id).get();
                correction.setCorrectionDays(correction.getCorrectionDays());
                correction.setCorrection(correction.getCorrection());
                correction.setMembers(correction.getMembers());
                Correction correction1=correctionService.updateCorrection(correction);
                return new ResponseEntity<>(correction1, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

