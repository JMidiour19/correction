package com.example.demo.controller;

import com.example.demo.dto.MemberDto;
import com.example.demo.mapper.MapperMember;
import com.example.demo.model.Member;
import com.example.demo.service.MemberService;
import com.example.demo.service.MemberServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/members")
public class MemberController {
    @Autowired
    MemberService memberService=new MemberServiceImpl();

    @PostMapping(value = "/addMember")
    public ResponseEntity<Member>  createMember(@RequestBody Member member){
        try {
         Member member1=memberService.createMember(member);
        return new ResponseEntity<>(member1,HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/listMember")
    public ResponseEntity<List<MemberDto>> findAllMember() {
        try {
            List<Member> members = this.memberService.findAllMember();
            List<MemberDto> membersDTO = new ArrayList<>();
            for (Member member : members) {
                membersDTO.add(MapperMember.memberToMemberDto(member));
            }
            return new ResponseEntity<>(membersDTO, HttpStatus.ACCEPTED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping( value="/listMember/{id}")
    public  ResponseEntity<Member> findMemberById(@PathVariable("id") Integer id) {
        try {
            Optional<Member> member = memberService.findMemberById(id);
            if (member.isPresent()) {
                return new ResponseEntity(member,HttpStatus.FOUND);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(value = "/deleteMember/{id}")
    public ResponseEntity<Member> deleteMemberById(@PathVariable Integer id){
        try {
            if(memberService.findMemberById(id).isPresent()) {
                memberService.deleteMemberById(id);
                return new ResponseEntity<>(HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "/updateMember/{id}")
    public ResponseEntity<Member> updateMember(@RequestBody Member member, @PathVariable Integer id) {
        try {
            if (memberService.findMemberById(id).isPresent()) {
                memberService.findMemberById(id).get();
                member.setNom(member.getNom());
                member.setPrenom(member.getPrenom());
                member.setUsername(member.getUsername());
                member.setPassword(member.getPassword());
                Member member1=memberService.updateMember(member);
                return new ResponseEntity<>(member1, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

