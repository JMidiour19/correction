package com.example.demo.service;

import com.example.demo.dto.CorrectionMemberDto;
import com.example.demo.dto.MemberDto;
import com.example.demo.model.Correction;
import com.example.demo.model.Member;
import com.example.demo.repository.CorrectionRepository;
import com.example.demo.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class MemberServiceImpl implements MemberService{
    @Autowired
    MemberRepository memberRepository;
    @Override
    public Member createMember(Member member) {
        return memberRepository.save(member);
    }

    @Override
    public Member updateMember(Member member) {
        return memberRepository.save(member);
    }

    @Override
    public Optional<Member> findMemberById(Integer id) {
        return memberRepository.findById(id);
    }

    @Override
    public List<Member> findAllMember() {
        List<Member> result= (List<Member>) memberRepository.findAll();
        return result;
    }

    @Override
    public void deleteMemberById(Integer id) {
        memberRepository.deleteById(id);
    }
}
