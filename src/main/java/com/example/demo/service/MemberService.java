package com.example.demo.service;

import com.example.demo.model.Member;
import java.util.List;
import java.util.Optional;

public interface MemberService {
    Member createMember(Member member);
    Member updateMember(Member member);
    Optional<Member> findMemberById(Integer id);
    List<Member> findAllMember();
    void deleteMemberById(Integer id);
}
