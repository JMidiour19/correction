package com.example.demo.service;

import com.example.demo.model.Correction;
import com.example.demo.repository.CorrectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class CorrectionServiceImpl implements CorrectionService{
    @Autowired
    CorrectionRepository correctionRepository;
    @Override
    public Correction createCorrection(Correction correction) {
        return correctionRepository.save(correction);
    }

    @Override
    public Correction updateCorrection(Correction correction) {
        return correctionRepository.save(correction);

    }

    @Override
    public Optional<Correction> findCorrectionById(Integer id) {
        return correctionRepository.findById(id);
    }

    @Override
    public List<Correction> findAllCorrection() {
       List<Correction> result = (List<Correction>) correctionRepository.findAll();

       for(Correction correction : result) {
            if(null != correction.getMembers()){
                int memberId = correction.getMembers().getId();
                correction.setMemberId(memberId);
            }
        }
        return result;
    }

    @Override
    public void deleteCorrectionById(Integer id) {
        correctionRepository.deleteById(id);

    }
}
