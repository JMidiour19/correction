package com.example.demo.dto;

import lombok.Data;
import java.util.List;

@Data
public class MemberDto {
    private Integer id;
    private String nom;
    private String prenom;
    private String username;
    private String password;
    private List<CorrectionMemberDto> corrections;
}
