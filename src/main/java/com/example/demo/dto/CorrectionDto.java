package com.example.demo.dto;

import lombok.Data;
import java.time.LocalDate;

@Data
public class CorrectionDto {

    private Integer id;
    private LocalDate correctionDays;
    private Double correction;
    private Integer memberId;
}
