package com.example.demo.dto;

import lombok.Data;
import java.time.LocalDate;

@Data
public class CorrectionMemberDto {
    private Integer id;
    private LocalDate correctionDays;
    private Double correction;

}
