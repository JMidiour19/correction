package com.example.demo.mapper;

import com.example.demo.dto.CorrectionMemberDto;
import com.example.demo.dto.MemberDto;
import com.example.demo.model.Correction;
import com.example.demo.model.Member;
import java.util.ArrayList;
import java.util.List;

public class MapperMember {
    public static MemberDto memberToMemberDto(Member member) {
        MemberDto memberDto = new MemberDto();
        memberDto.setId(member.getId());
        memberDto.setNom(member.getNom());
        memberDto.setPrenom(member.getPrenom());
        memberDto.setUsername(member.getUsername());
        memberDto.setPassword(member.getPassword());


        List<CorrectionMemberDto> result = new ArrayList<>();
        CorrectionMemberDto corrections = new CorrectionMemberDto();
        List<Correction> cor = member.getCorrections();

        for (Correction correction : cor) {
            corrections.setId(correction.getId());
            corrections.setCorrectionDays(correction.getCorrectionDays());
            corrections.setCorrection(correction.getCorrection());
            result.add(corrections);
        }
        memberDto.setCorrections(result);
        return memberDto;
    }
}
