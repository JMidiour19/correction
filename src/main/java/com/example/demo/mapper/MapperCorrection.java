package com.example.demo.mapper;

import com.example.demo.dto.CorrectionDto;
import com.example.demo.model.Correction;

public class MapperCorrection {
    public static Correction correctionToCorrectionDto (CorrectionDto correctionDto) {
        Correction correction = new Correction();
        correction.setId(correctionDto.getId());
        correction.setCorrectionDays(correctionDto.getCorrectionDays());
        correction.setCorrection(correctionDto.getCorrection());
        return correction;
    }
}

